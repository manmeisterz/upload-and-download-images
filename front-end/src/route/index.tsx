import React, { useState, useEffect } from "react";
import { saveAs } from "file-saver";
import axios from "axios";
const Index = () => {
  var [file, setfile] = useState("");
  var [password, setPassword] = useState("");
  var [preview, setPreview] = useState(null);

  const handleuploads = (event: any) => {
    const images = event.target.files[0] || "";

    setfile(images);
    setPreview(null);
  };

  const handleDownloads = () => {
    axios
      .post("http://localhost:8000/Download", { password: password })
      .then((result) => {
        if (result.status === 200) {
          let images = result.data.data;
          console.log(images.Url);
          setPreview(images.Url);
          saveAs(images.Url, images.filename);
        }
      })
      .catch((error) => {
        console.log(error);
        let message = error.response.data.data;
        alert(message);
        setPreview(null);
      });
  };

  const submituploads = () => {
    let formData = new FormData();
    // console.log(file)
    if (file === "") {
      // console.log(file)
      alert("กรุณาเลือกรูปภาพ");
      return false;
    }

    if (password === "") {
      alert("กรุณาใส่รหัสผ่าน");
      return false;
    }

    formData.append("file", file);
    formData.append("password", password);
    // console.log(file,password)
    const config = {
      headers: { "content-type": "multipart/form-data" },
    };
    axios
      .post("http://localhost:8000/uploads", formData, config)
      .then((result: any) => {
        if (result.status === 200) {
          alert("Upload รูปภาพสำเร็จ");
        }
      })
      .catch((error) => {
 
        let message = error.response.data.data;
        alert(message);
      });
  };
  // useEffect(()=>)
  return (
    <div className="container h-100 w-100">
      <div className="row">
        {/* <div className='col'></div> */}
        <div className="col-md-6  position-absolute top-50 start-50 translate-middle ">
          <img
            src={preview!}
            style={
              preview == null
                ? { display: "none" }
                : { width: "500px", height: "500px" }
            }
            alt="Downloads"
          />
          <div className="row">
            <div className="col-4">
              <label htmlFor="password">Images:</label>
              <input
                type="file"
                onChange={handleuploads}
                className="form-control"
              />
            </div>
            <div className="col-4">
              <label htmlFor="password">Password:</label>
              <input
                type="text"
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                className="form-control"
              />
            </div>
            <div className="col-2">
              <label htmlFor=""></label>
              <button className="form-control" onClick={submituploads}>
                Uploads
              </button>
            </div>
            <div className="col-2">
              <label htmlFor=""></label>
              <button className="form-control" onClick={handleDownloads}>
                Downloads
              </button>
            </div>
          </div>
        </div>
        {/* <div className='col'></div> */}
      </div>
    </div>
  );
};

export default Index;
