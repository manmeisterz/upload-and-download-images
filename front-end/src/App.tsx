import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Routes,Route,Link  } from 'react-router-dom';
import Index from './route/index';
function App() {
  return (
      <>
        <Routes>
          <Route index path='/' element={<Index/>}></Route>
        </Routes>
      </>
  );
}

export default App;
