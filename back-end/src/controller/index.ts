
import multer from 'multer';
import { nanoid } from 'nanoid';
import { Uploads } from '../db/connect';

export class Index {
    async Uploads(req:any,res:any){
        let {password} = req.body
        let checkpassword = await Uploads.findOne({password:password})
    
        if(checkpassword == null){
    
            Uploads.create({filename:req.file!.filename,Url:`${req.protocol}://${req.get('host')}/uploads/${req.file!.filename}`,password:password})
            res.sendStatus(200)
        }else{
            res.status(400).send({data:"มีรหัสผ่านนี้อยู่แล้ว"})
        }
    }


    async Downloads (req:any,res:any){
        let {password} = req.body
  
        let images = await Uploads.findOne({password:password})
    
        if(images == null){
            res.status(400).send({data:"ไม่พบข้อมูล"})
        }else{
    
            res.status(200).send({data:images})
        }
    }


}