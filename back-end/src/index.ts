import express, { Application, Request, Response } from 'express';
import multer from 'multer';
import { nanoid } from 'nanoid';
import { Uploads } from './db/connect';
import cors from 'cors'
import bodyParser from 'body-parser'
import { Index } from './controller';
const app:Application = express();
const PORT = 8000;

const corsOptions ={
    origin:'*', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200
  }

app.use(cors(corsOptions))
app.use( bodyParser.json()); 
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
app.use(express.static('./src/public/'))
type DestinationCallback = (error: Error | null, destination: string) => void
type FileNameCallback = (error: Error | null, filename: string) => void

export const fileStorage = multer.diskStorage({
    destination: (
        request: Request,
        file: Express.Multer.File,
        callback: DestinationCallback
    ): void => {
        callback(null,'./src/public/uploads')
    },

    filename: (
        req: Request, 
        file: Express.Multer.File, 
        callback: FileNameCallback
    ): void => {
        let id =  nanoid();
        callback(null,`${id}-test.jpg`)
    }
})

const uploads = multer({storage:fileStorage});

app.get('/', (req:Request, res:Response) => res.send('Express + TypeScript Server'));

app.post('/uploads',uploads.single('file'), async (req:Request,res:Response)=>{ new Index().Uploads(req,res)})

app.post('/Download',async (req:Request,res:Response)=>{ new Index().Downloads(req,res) })

app.listen(PORT, () => {
  console.log(`Server is running at http://localhost:${PORT}`);
});