// const { MongoClient, ServerApiVersion } = require('mongodb');
import mongoose from 'mongoose';
import * as dotenv from 'dotenv';
const { Schema } = mongoose;
require('dotenv').config();
mongoose.connect(process.env.mongodb!)

const UploadSchema = new Schema({
    _ID:  mongoose.Schema.Types.ObjectId,
    filename: String,
    Url:String,
    password: String
})

export const Uploads = mongoose.model('Uploads', UploadSchema);
